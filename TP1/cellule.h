#ifndef __CELLULE_H__
#define __CELLULE_H__

typedef struct Cellule {
    int id_sommet;
    struct Cellule *pred;
    struct Cellule *succ;
}cellule_t;

void initialiser_cellule(cellule_t *, int); //int / void -> changer valeur de retour ?

#endif
