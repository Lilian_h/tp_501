/*  
    PARTIE 1 
    TP 1 
    HOLLARD LILIAN 
*/
#include "liste.h"
#include <stdlib.h>
#include <stdio.h>
#include "graphe.h"

//préciser le nombre de valeur a ajouter dans le graphe
int main(int argc, char* argv[]){
    (void)argc;
    
    graphe_t* g = initialiser_graphe(argv[1]);
    printf("Nombre de sommets : %d\n", g->n_sommets);
    if(g->oriente != 0){
        printf("oriente\n");
    }else{
        printf("non oriente\n");
    }
    if(g->valeur != 0){
        printf("value\n");
    }else{
        printf("Non value\n");
    }

    printf("Listes d'ajacences\n");
    afficher_graphe_liste_adj(g);
    printf("\nMatrice d'adjacences:\n");
    afficherMatrice(g);
    detruire_graphe(g);
    return 0;
}
