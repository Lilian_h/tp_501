#ifndef __GRAPHE_H__
#define __GRAPHE_H__
#include "liste.h"

typedef struct {
    int n_sommets;
    int oriente;
    int valeur;
    liste_t** l_adj;
    int** m_adj;
    int* m_stockage;
}graphe_t;

graphe_t* initialiser_graphe(char* nomFichier);
int detruire_graphe(graphe_t *g);
void afficher_graphe_liste_adj(graphe_t *g);
void afficherMatrice(graphe_t* g);

#endif
