#ifndef __GRAPHE_H__
#define __GRAPHE_H__
#include "cellule.h"


typedef struct Sommet_graphe{
	int valeur;
	int distance;
	int couleur;
	int dateDeb;
	int dateFin;
    struct Sommet_graphe *pred;
}sommet_graphe_t;

typedef struct {
	int n_sommet;
	int n_arete;
	int date;
	int valeur;
	int **mat;
	cellule_t **adj;
}graphe_t;


typedef struct Arete {
    int sommet_u;
    int sommet_v;
    int poids;
}arete_t;

graphe_t* construire_graphe(const int);
void detruire_graphe(graphe_t*);
void inserer(graphe_t*, int, cellule_t*);
void afficher_graphe(graphe_t*);
graphe_t* construire_graphe_depuis_texte(const char *);

void construire_tableau_arete(graphe_t* g, arete_t* t_a);

//TP 7 :
int rechercher_poids_arete(graphe_t*, int, int);
sommet_graphe_t* source_unique_initialisation(graphe_t*, int);
void relacher(sommet_graphe_t*, int, int, int);
sommet_graphe_t* bellman_ford(graphe_t*, int); // 2nd param -> sommet de départ
//void afficher_chemin(graphe_t*, sommet_graphe_t*, int);
void afficher_chemin_recursif(graphe_t*, int, int, sommet_graphe_t *);
void afficher_chemin(graphe_t*, sommet_graphe_t *, int);

//Dijkstra
void diminuer_sommet(sommet_graphe_t*, int, int, int);
void tri_sommet(sommet_graphe_t*, int);
sommet_graphe_t extraire_min(sommet_graphe_t*, int);
sommet_graphe_t* dijkstra(graphe_t*, int); // graphe - w - s

//plus courts chemin
sommet_graphe_t** pcc_tous_a_tous(graphe_t*);

//Floyd Warshall
int** floyd_warshall(int**, int);

#endif
