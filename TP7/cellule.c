#include "cellule.h"
#include <stdlib.h>

/*
    Construire_cellule : 
    construction d'un sommet ayant une valeur et un poids.
*/
cellule_t* construire_cellule(const int id_sommet, const int poids){
	cellule_t *sommet = (cellule_t*) malloc(sizeof(cellule_t));

	sommet->id_sommet = id_sommet;
	sommet->poids = poids;
	sommet->succ = NULL;

	return sommet;
}

/*
    detruire_cellule. 
*/
void detruire_cellule(cellule_t* sommet)
{
	free(sommet);
	sommet = NULL;
}
