
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"
#include "file.h"

/*
    Construire_graphe : 
    construction d'un graphe vide à partir de n_sommet
*/
graphe_t* construire_graphe(const int n_sommet){
	graphe_t* g = (graphe_t*) malloc(sizeof(graphe_t));

	g->n_sommet = n_sommet;
	g->n_arete = 0;
	g->date = 0;
	g->valeur = 0;

	g->adj = (cellule_t**) malloc(sizeof(cellule_t*) * n_sommet);

	return g;
}


/*
    detruire_graphe.
*/
void detruire_graphe(graphe_t* g){
	cellule_t* cellule_temp = NULL;

	for(int i = 0; i < g->n_sommet; i ++){
		while(g->adj[i] != NULL)
		{
			cellule_temp = g->adj[i]->succ;
			free(g->adj[i]);
			g->adj[i] = cellule_temp;
		}
	}

	free(g);
	g = NULL;
}

/*
    inserer :
    inserer un sommet dans un graphe depuis un indice dans la liste
    d'adjacence.
*/
void inserer(graphe_t* g, int indice, cellule_t* sommet)
{
	sommet->succ = g->adj[indice]->succ;
	g->adj[indice]->succ = sommet;
}

/*
    Anciennement : afficher_liste appelé iterativement
    Nouvelle version : 
        - > affichage de chaque cellule_t (aka sommet) de la liste d'adjacence
        - > continuer pour chaque indice du tableau de liste d'adj
*/
void afficher_graphe(graphe_t* g)
{
	for(int i = 0; i < g->n_sommet; i ++)
	{
		if(g->adj[i] != NULL)
		{
			cellule_t *sommet = g->adj[i];
			printf("[%d] -> ", sommet->id_sommet);

			while(sommet->succ != NULL)
			{
				if(g->valeur)
					printf("%d: poids(%d); ", sommet->succ->id_sommet, sommet->succ->poids);
				else
					printf("%d ", sommet->succ->id_sommet);
				sommet = sommet->succ;
			}	
		}
		printf("\n");	
	}
}

/* 
    Nouvelle version : identique à la précédente, mais réadapté 
	Ajout : matrice n x n pour Floyd warshall
*/
graphe_t* construire_graphe_depuis_texte(const char * nomFichier)
{
	FILE *file = NULL;
	char propriete[20];
	int valeur = 0;
	int n_arete = 0;
	int boolDefAretes = 0;

	int nbSommet = 0;
	int oriente = 0;
	int value = 0;

	graphe_t* g = NULL;
	
	file = fopen(nomFichier, "r");

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "nSommets") == 0)
		nbSommet = valeur;

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "oriente") == 0)
		oriente = valeur;

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "value") == 0)
		value = valeur;


	g = construire_graphe(nbSommet);

	g->valeur = value;

	for(int i = 0; i < nbSommet; i++){
		g->adj[i] = construire_cellule(i, -1);
	}

	g->mat = (int**)malloc(sizeof(int*)*nbSommet);
	for(int i = 0; i < nbSommet; i++){
			g->mat[i] = (int*)calloc(g->n_sommet, sizeof(int));	
	}

	while(! feof(file)){
		fscanf(file, "%s", propriete);
		
		if(strcmp(propriete, "DEBUT_DEF_ARETES") == 0)
			boolDefAretes = 1;
	
		else if(strcmp(propriete, "FIN_DEF_ARETES") == 0)
			boolDefAretes = 0;

		else if(boolDefAretes){	
			fseek(file, strlen(propriete) * - 1, SEEK_CUR);

			int initial, terminal, poids;

			if(value){
				fscanf(file, "%d %d %d", &initial, &terminal, &poids);
			}
			else{
				fscanf(file, "%d %d", &initial, &terminal);
				poids = -1;
			}	

			inserer(g, initial,construire_cellule(terminal, poids));
			g->mat[initial][terminal] = poids;
			
			if(!oriente)
				inserer(g, terminal,construire_cellule(initial, poids));
				g->mat[terminal][initial] = poids;

			n_arete ++;
		}	
	}

	g->n_arete = n_arete;
	fclose(file);

	return g;
}


void construire_tableau_arete(graphe_t* g, arete_t* t_a){
    int compteur = 0;
	for(int i = 0; i < g->n_sommet; i ++)
	{	
		cellule_t *sommet = g->adj[i];
		while(sommet->succ != NULL)
		{
			arete_t arete = {g->adj[i]->id_sommet, sommet->succ->id_sommet, sommet->succ->poids};
			t_a[compteur++] = arete;
			sommet = sommet->succ;
		}
	}	
}

int rechercher_poids_arete(graphe_t* g, int u, int v){
    int res = -1;
    cellule_t *sommet = g->adj[u];
	while(sommet->succ != NULL){
		if(sommet->succ->id_sommet == v)
			res = sommet->succ->poids;
		sommet = sommet->succ;
	}
	return res;
}

sommet_graphe_t* source_unique_initialisation(graphe_t* g, int s){
    sommet_graphe_t* res = (sommet_graphe_t*)malloc(sizeof(sommet_graphe_t)*g->n_sommet);
    for(int i = 0; i < g->n_sommet; i++){
        res[i].valeur = i;
        res[i].distance = 9999;
        res[i].pred = NULL;
    }

    res[s].distance = 0;
    return res;
}

void relacher(sommet_graphe_t* s, int u, int v, int w){
    if(s[v].distance > s[u].distance + w){
        s[v].distance = s[u].distance + w;
        s[v].pred = &s[u];
    }
}

sommet_graphe_t* bellman_ford(graphe_t* g, int s){
    sommet_graphe_t* sommets = source_unique_initialisation(g, s);

    arete_t t_a[g->n_arete];
    for (int i = 0; i < g->n_arete; i++){
		t_a[i].sommet_u = -1;
		t_a[i].sommet_v = -1;
		t_a[i].poids = 10000;
	}

    construire_tableau_arete(g, (arete_t*)&t_a);

    for(int i = 0; i < g->n_sommet - 1; i++){
        for(int j = 0; j < g->n_arete; j++){
            int u = t_a[j].sommet_u;
            int v = t_a[j].sommet_v;
            int poids = t_a[j].poids;

            //Relacher
			relacher(sommets, u, v, poids);
        }
    }

    for (int i = 0; i < g->n_arete-1; i++)
	{
		int u = t_a[i].sommet_u;
		int v = t_a[i].sommet_v;
		int poids = t_a[i].poids;

		if(sommets[v].distance > sommets[u].distance + poids)
			return NULL;
	}
    return sommets;
}


void afficher_chemin_recursif(graphe_t* g, int i, int j, sommet_graphe_t *s){
    if(i == j){}
	else if(s[j].pred == NULL){
		printf("Pas de chemin : %d\n", j);
	}
	else{
		afficher_chemin_recursif(g, i, s[j].pred->valeur, s);

		int poids = rechercher_poids_arete(g, s[j].pred->valeur, j);

		printf("[%d] -> [%d] = %d >>> ", s[j].pred->valeur, j, poids);
	}
}

void afficher_chemin(graphe_t* g, sommet_graphe_t *s, int i){
    printf("Descriptif : sommet_depart -> v(poids) : ");
	printf("\n------------\n");
	for (int j = 0; j < g->n_sommet; j++){
		printf("[%d] -> %d(%d) : \t", i, j, s[j].distance);
		afficher_chemin_recursif(g, i, j, s);
		printf("\n------------\n");
	}
}

void tri_sommet(sommet_graphe_t* s, int taille){
	sommet_graphe_t temp;
	int j = 0;
	for(int i = 1; i < taille; i++){
		temp = s[i];
		for(j = i; j > 0 && s[j-1].distance > temp.distance; j--){
			s[j] = s[j-1];
		}
		s[j] = temp;
	}
}

sommet_graphe_t extraire_min(sommet_graphe_t* s, int taille){
	sommet_graphe_t res = s[0];
	s[0] = s[taille];
	s[taille] = res;

	tri_sommet(s, taille - 1);
	return res;
}

void diminuer_sommet(sommet_graphe_t* t, int taille, int s, int d){
	for(int i = 0; i < taille; i++){
			if(t[i].valeur == s){
				t[i].distance = d;
			}
		}
	tri_sommet(t, taille);
}

sommet_graphe_t* dijkstra(graphe_t* g, int s){
	sommet_graphe_t* sommets = source_unique_initialisation(g, s); //res
	sommet_graphe_t* t = source_unique_initialisation(g, s);
	int taille = g->n_sommet;

	while(taille > 0){
		taille--;
		sommet_graphe_t u = extraire_min(t, taille);
		cellule_t* v = g->adj[u.valeur];
	

		while(v->succ != NULL){
			int val_u = u.valeur;
			int val_v = v->succ->id_sommet;
			int w = v->succ->poids;

			if(sommets[val_v].distance > sommets[val_u].distance + w){
				sommets[val_v].distance = sommets[val_u].distance + w;
				sommets[val_v].pred = &sommets[val_u];
				
				diminuer_sommet(t, taille, val_v, sommets[val_u].distance + w);

			}
			v = v->succ;
		}
	}
	free(t);

	return sommets;
}

sommet_graphe_t** pcc_tous_a_tous(graphe_t* g){
	sommet_graphe_t** res = (sommet_graphe_t**)malloc(sizeof(sommet_graphe_t*)*g->n_sommet);
	for(int i = 0; i < g->n_sommet; i++){
		res[i] = dijkstra(g, i);
	}
	return res;
}

int** floyd_warshall(int** P, int taille){
	int** D = malloc(sizeof(int*)*taille);

	/*Initialisation de D0*/
    for (int i = 0; i < taille; i++) {
		D[i] = malloc(sizeof(int)*taille);
		for (int j = 0; j < taille; j++) {
			if(P[i][j] == 0){
				D[i][j] = 9999;
			}else{
				D[i][j] = P[i][j];
			}
		}	
    }

	for(int k = 0; k < taille; k++){
		for(int i = 0; i < taille; i++){
			for(int j = 0; j < taille; j++){
				if(D[i][j] > D[i][k] + D[k][j]){
					D[i][j] = D[i][k] + D[k][j];
 				}
			}
		}
	}
	return D;
}