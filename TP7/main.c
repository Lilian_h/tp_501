//
//  main.c
//  TP7_501
//
//  Created by Lilian Hollard on 27/11/2019.
//  Copyright © 2019 Lilian Hollard. All rights reserved.
//

#include <stdio.h>
#include "graphe.h"

int main(int argc, char *argv[]) {
    (void)argc;
    graphe_t* graph_bellman = construire_graphe_depuis_texte(argv[1]);
    graphe_t* graph_dijkstra = construire_graphe_depuis_texte(argv[2]);
    //afficher_graphe(g);
    sommet_graphe_t* s = bellman_ford(graph_bellman, 0);
    afficher_chemin(graph_bellman, s, 0);

    /*sommet_graphe_t* dij = dijkstra(graph_dijkstra, 0);
    afficher_chemin(graph_dijkstra, dij, 0);*/
    sommet_graphe_t** ppc = pcc_tous_a_tous(graph_dijkstra);
    int choix;
    printf("\n\nVeuillez choisir un sommet de départ :\n");
    scanf("%d", &choix);
    afficher_chemin(graph_dijkstra, ppc[choix], choix);
  
    printf("\n\n Floyd-Warshall\n");
    int** D = floyd_warshall(graph_dijkstra->mat, graph_dijkstra->n_sommet);
    for(int i = 0; i < graph_dijkstra->n_sommet; i++){
        for(int j = 0; j < graph_dijkstra->n_sommet; j++){
            if(i == j){
                printf("[ 0]");
            }else{
                printf("[%2d] ", D[i][j]);
            }
        }   
        printf("\n");
    }
    
    return 0;
}
