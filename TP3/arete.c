#include "arete.h"

aretes_graphe_t* recup_arete(graphe_t G){
    int taille = 0;
   
    for(int i = 0; i < G.n_sommets; i++){
        for(int j = 0; j < G.n_sommets; j++){
            if(G.m_adj[i][j] > 0){
                taille++;
            }
        }
    }

    taille = taille/2;
    aretes_graphe_t* res = malloc(sizeof(aretes_graphe_t));
    arete_t* temp = malloc(sizeof(arete_t)*taille);
    res->aretes = malloc(sizeof(arete_t)*taille);
    res->taille = taille;
    int compteur_arete = 0;
    for(int i = 0; i < G.n_sommets; i++){
        for(int j = 0; j < G.n_sommets; j++){
            if(G.m_adj[i][j] > 0){
                int cond = 0;
                for(int x = 0; x < compteur_arete; x++){
                    if(temp[x].poids == G.m_adj[i][j] && temp[x].sommet_u == j && temp[x].sommet_v == i){
                        cond = 1;
                    }
                }
                if(cond == 0){
                    temp[compteur_arete].poids = G.m_adj[i][j];
                    temp[compteur_arete].sommet_u = i;
                    temp[compteur_arete].sommet_v = j;
                    compteur_arete++;
                }
            }
        }
    }
    printf("compteur arete:%d\n", compteur_arete);
    res->aretes = temp;
    res->longueur = res->taille;
    return res;
}

