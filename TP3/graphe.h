#ifndef __GRAPHE_H__
#define __GRAPHE_H__
#include "liste.h"


enum COULEUR {
    BLANC, 
    GRIS,
    NOIR
};
typedef struct Sommet{
    int val;
    int couleur;
    int distance;
    int date_deb;
    int date_fin;
    struct Sommet* pere;
}sommet_t;

typedef struct {
    int n_sommets;
    int oriente;
    int valeur;
    liste_t** l_adj;
    int** m_adj;
    int* m_stockage;
    sommet_t* sommets;
}graphe_t;

graphe_t* initialiser_graphe(graphe_t *g, char* nomFichier);
int detruire_graphe(graphe_t *g);
void afficher_graphe_liste_adj(graphe_t *g);
void afficherMatrice(graphe_t* g);

#endif
