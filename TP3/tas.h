#ifndef __TAS_H__
#define __TAS_H__
#include "arete.h"



int parent(int);
int gauche(int);
int droite(int);
void echanger(aretes_graphe_t*, int, int);
void entasser_max(aretes_graphe_t*, int);
void construire_tas_max(aretes_graphe_t*);






#endif