#ifndef __ARETE_H__
#define __ARETE_H__
#include "graphe.h"
#include <stdio.h>
#include <stdlib.h>

/*Définissez une structure de donénes appropriée pour représenter une arête*/
typedef struct Arete{
    int sommet_u;
    int sommet_v;
    int poids;
}arete_t;

typedef struct Aretes_graphe{
    arete_t* aretes;
    int taille;
    int longueur; //gestion des tas
}aretes_graphe_t;

/*Écrivez un code permettant de récupérer toutes les arêtes du graphe à partir de ses listes d'adjacences ou de sa matrice d'adjacences.*/
aretes_graphe_t* recup_arete(graphe_t G);

#endif
