#include "liste.h"
#include <stdio.h>
#include <stdlib.h>

void initialiser_liste(liste_t* l){
    l->tete = NULL;
}

void detruire_liste(liste_t* l){
    cellule_t* temp = NULL;
    while(l->tete != NULL){
        temp = l->tete->succ;
        free(l->tete);
        l->tete = temp;
    }
    free(l);
    l = NULL;
}

int inserer_liste(liste_t* l, cellule_t* c){
    int res = 1;
    if(rechercher_liste(l, c->id_sommet) == NULL){ //si l'element n'est pas présent dans la liste
        c->succ = l->tete;
        if(l->tete != NULL)
			l->tete->pred = c;
		
		l->tete = c;
		c->pred = NULL;
        res = 0;
    }
    return res;
}

cellule_t* rechercher_liste(liste_t* l, int id_sommet){
    cellule_t* c = l->tete;
    while(c != NULL && c->id_sommet != id_sommet)
		c = c->succ;
	return c;
}

void supprimer_cellule(liste_t* l, cellule_t* c){
    if(c->pred != NULL){
        c->pred->succ = c->succ; 
        //le pointeur de mon prédecesseur vers son successeur, pointe désormais sur mon successeur
    }else{
        l->tete = c->succ;
        //si je n'ai pas de prédecesseur, je suis en tête -> la tête pointe donc maintenant vers mon successeur
    }
    
    if(c->succ != NULL){
        c->succ->pred = c->pred;
        //le p de mon pred vers son succ pointe désormais sur mon pred;
    }
}

void afficher_liste(liste_t* l)
{
    cellule_t* c;
    c = l->tete;
    while(c != NULL)
    {
        printf("%d  ", c->id_sommet);
        c = c->succ;
    }
    printf("\n");
}
