#include "graphe.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


graphe_t* initialiser_graphe(graphe_t* g, char* nomFichier){
    FILE* fichier = NULL;
    char propriete[20];
    int valeur;
    int nbSommet, oriente, value;
    int boolDefAretes;

  
    /* Definition des propriétés du graphe */
    fichier = fopen(nomFichier, "r");
    fscanf(fichier, "%s %d", propriete, &valeur);
    if(strcmp(propriete, "nSommets") == 0)
        nbSommet = valeur;
    fscanf(fichier, "%s %d", propriete, &valeur);
    if(strcmp(propriete, "oriente") == 0)
        oriente = valeur;
    fscanf(fichier, "%s %d", propriete, &valeur);
    if(strcmp(propriete, "value") == 0)
        value = valeur;
    
    g->l_adj = (liste_t**)malloc(sizeof(liste_t*));
    g->n_sommets = nbSommet;
    g->oriente = oriente;
    g->valeur = value;
    
    g->sommets = (sommet_t*)malloc(sizeof(sommet_t)*nbSommet);
    for(int i = 0; i < nbSommet; i++){
        enum COULEUR c = BLANC;
        g->sommets[i].val = i;
        g->sommets[i].couleur = c;
        g->sommets[i].distance = 10000;
        g->sommets[i].pere = NULL;
        g->sommets[i].date_deb = 10000;
        g->sommets[i].date_fin = 10000;
    }



    g->m_adj = (int**)malloc(sizeof(int*) * nbSommet);
    for (int i = 0; i < nbSommet; i++){
		g->m_adj[i] = (int*)calloc(nbSommet, nbSommet);
	}
    for(int i = 0; i < nbSommet; i++){
        for(int j = 0; j < nbSommet; j++){
            g->m_adj[i][j] = 0;
        }
    }
    /*---------------------------------------*/
    
    for(int i = 0; i < g->n_sommets; i++){
        g->l_adj[i] = (liste_t*)malloc(sizeof(liste_t));
        initialiser_liste(g->l_adj[i]);
    }


            
    while(! feof(fichier))
	{
		fscanf(fichier, "%s", propriete);
		
		if(strcmp(propriete, "DEBUT_DEF_ARETES") == 0)
			boolDefAretes = 1;
	
		else if(strcmp(propriete, "FIN_DEF_ARETES") == 0)
			boolDefAretes = 0;

		else if(boolDefAretes)
		{	
			fseek(fichier, strlen(propriete) * - 1, SEEK_CUR);

			int initial, terminal, poids;

			if(value == 1)
			{
				fscanf(fichier, "%d %d %d", &initial, &terminal, &poids);
			}
			else
			{
				fscanf(fichier, "%d %d", &initial, &terminal);
				poids = 1;
			}	
           
            g->m_adj[initial][terminal] = poids;
            g->m_adj[terminal][initial] = poids;
            
            

           
            cellule_t* c = malloc(sizeof(cellule_t));
            initialiser_cellule(c, terminal, poids);

            int res = inserer_liste(g->l_adj[initial], c);

          
            cellule_t* c2 = malloc(sizeof(cellule_t));
            initialiser_cellule(c2, initial, poids);
            res = inserer_liste(g->l_adj[terminal], c2);
            
		}	
	}
  


	fclose(fichier);
    return g;
}

void afficher_graphe_liste_adj(graphe_t *g){
    for(int i = 0; i < g->n_sommets; i++){
        printf("%d-->", i);
        afficher_liste(g->l_adj[i]);
    }
}

void afficherMatrice(graphe_t* g)
{
	for (int initial = 0; initial < g->n_sommets; initial++)
	{
		for (int terminal = 0; terminal < g->n_sommets; terminal++)
		{
			printf("%d ", g->m_adj[initial][terminal]);
		}
		printf("\n");
	}
}

int detruire_graphe(graphe_t *g){
    for(int i = 0; i < g->n_sommets; i++){
		free(g->m_adj[i]);
		g->m_adj[i] = NULL;
	}

	free(g->m_adj);
	g->m_adj = NULL;

    free(g->sommets);
    g->sommets = NULL;
    
    free(g);
    g = NULL;
    
    return 0;
}