#include "tri.h"
#include "tas.h"

void tri_insertion(aretes_graphe_t* tab_a){
    int cle;
    int i;
    for(int j = 1; j < tab_a->taille; j++){
        cle = tab_a->aretes[j].poids;
        i = j - 1;
        while(i >= 0 && tab_a->aretes[i].poids > cle){
            tab_a->aretes[i+1] = tab_a->aretes[i];
            i = i - 1;
        }
        tab_a->aretes[i+1].poids = cle;
    }
}



void tri_par_tas(aretes_graphe_t* tab_a){
    construire_tas_max(tab_a);
    for(int i = tab_a->taille - 1; i > 0; i--){
        echanger(tab_a, i, 0);
        tab_a->taille--;
        entasser_max(tab_a, 0);
    }
}