#ifndef __LISTE_H__
#define __LISTE_H__
#include "cellule.h"

typedef struct Liste {
    cellule_t* tete;
}liste_t;

void initialiser_liste(liste_t*);
void detruire_liste(liste_t*);
int inserer_liste(liste_t*, cellule_t*);
void supprimer_cellule(liste_t*, cellule_t*);
cellule_t* rechercher_liste(liste_t*, int);
void afficher_liste(liste_t*);

#endif
