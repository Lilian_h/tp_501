#include "tas.h"

int parent(int i){
    return (i-1)/2; 
}

int gauche(int i){
    return ((i*2)+1);
}

int droite(int i){
    return ((i*2)+2);
}

void echanger(aretes_graphe_t* t, int a, int b){
    int poids_a = t->aretes[a].poids;
    int sommet_u_a = t->aretes[a].sommet_u;
    int sommet_v_a = t->aretes[a].sommet_v;

    t->aretes[a].poids = t->aretes[b].poids;
    t->aretes[a].sommet_u = t->aretes[b].sommet_u;
    t->aretes[a].sommet_v = t->aretes[b].sommet_v;

    t->aretes[b].poids = poids_a;
    t->aretes[b].sommet_u = sommet_u_a;
    t->aretes[b].sommet_v = sommet_v_a;
}

void entasser_max(aretes_graphe_t* t, int i){
    int g = gauche(i);
    int d = droite(i);
    int max = i;
    if(g < t->taille && t->aretes[g].poids > t->aretes[i].poids){
        max = g;
    }
    if(d < t->taille && t->aretes[d].poids > t->aretes[max].poids){
        max = d;
    }
    if(max != i){
        echanger(t, i, max);
        entasser_max(t, max);
    }
}

void construire_tas_max(aretes_graphe_t* t){
    t->taille = t->longueur;
    for(int i = (int)(t->longueur/2); i >= 0; i--){
        entasser_max(t, i);
    }
}