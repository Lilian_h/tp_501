#include <stdio.h>
#include <stdlib.h>
#include "graphe.h"
#include "arete.h"
#include "tri.h"
#include "tas.h"

int main(int argc, char* argv[]){
    (void)argc;
    graphe_t* g = malloc(sizeof(graphe_t));
    initialiser_graphe(g, argv[1]);

    aretes_graphe_t* tab_arete_graphe = recup_arete(*g);
    for(int i = 0; i < tab_arete_graphe->taille; i++){
        printf("Poids : %d, u : %d, v: %d\n", tab_arete_graphe->aretes[i].poids, tab_arete_graphe->aretes[i].sommet_u, tab_arete_graphe->aretes[i].sommet_v);
    }
    printf("--------------------TRI INSERTION--------------------\n");
    tri_insertion(tab_arete_graphe);
    for(int i = 0; i < tab_arete_graphe->taille; i++){
        printf("Poids : %d, u : %d, v: %d\n", tab_arete_graphe->aretes[i].poids, tab_arete_graphe->aretes[i].sommet_u, tab_arete_graphe->aretes[i].sommet_v);
    }

    printf("--------------------TRI PAR TAS--------------------\n");
    tri_par_tas(tab_arete_graphe);

    for(int i = 0; i < tab_arete_graphe->longueur; i++){
        printf("Poids : %d, u : %d, v: %d\n", tab_arete_graphe->aretes[i].poids, tab_arete_graphe->aretes[i].sommet_u, tab_arete_graphe->aretes[i].sommet_v);
    }

    return 0;
}