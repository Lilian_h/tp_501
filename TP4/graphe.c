
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"
#include "ensemble.h"

/*
    Construire_graphe : 
    construction d'un graphe vide à partir de n_sommet
*/
graphe_t* construire_graphe(const int n_sommet){
	graphe_t* g = (graphe_t*) malloc(sizeof(graphe_t));

	g->n_sommet = n_sommet;
	g->n_arete = 0;
	g->date = 0;
	g->valeur = 0;

	g->adj = (cellule_t**) malloc(sizeof(cellule_t*) * n_sommet);

	return g;
}


/*
    detruire_graphe.
*/
void detruire_graphe(graphe_t* g){
	cellule_t* cellule_temp = NULL;

	for(int i = 0; i < g->n_sommet; i ++){
		while(g->adj[i] != NULL)
		{
			cellule_temp = g->adj[i]->succ;
			free(g->adj[i]);
			g->adj[i] = cellule_temp;
		}
	}

	free(g);
	g = NULL;
}

/*
    inserer :
    inserer un sommet dans un graphe depuis un indice dans la liste
    d'adjacence.
*/
void inserer(graphe_t* g, int indice, cellule_t* sommet)
{
	sommet->succ = g->adj[indice]->succ;
	g->adj[indice]->succ = sommet;
}

/*
    Anciennement : afficher_liste appelé iterativement
    Nouvelle version : 
        - > affichage de chaque cellule_t (aka sommet) de la liste d'adjacence
        - > continuer pour chaque indice du tableau de liste d'adj
*/
void afficher_graphe(graphe_t* g)
{
	for(int i = 0; i < g->n_sommet; i ++)
	{
		if(g->adj[i] != NULL)
		{
			cellule_t *sommet = g->adj[i];
			printf("[%d] -> ", sommet->id_sommet);

			while(sommet->succ != NULL)
			{
				if(g->valeur)
					printf("%d: poids(%d); ", sommet->succ->id_sommet, sommet->succ->poids);
				else
					printf("%d ", sommet->succ->id_sommet);
				sommet = sommet->succ;
			}	
		}
		printf("\n");	
	}
}

/* 
    Nouvelle version : identique à la précédente, mais réadapté 
	Ajout : matrice n x n pour Floyd warshall
*/
graphe_t* construire_graphe_depuis_texte(const char * nomFichier)
{
	FILE *file = NULL;
	char propriete[20];
	int valeur = 0;
	int n_arete = 0;
	int boolDefAretes = 0;

	int nbSommet = 0;
	int oriente = 0;
	int value = 0;

	graphe_t* g = NULL;
	
	file = fopen(nomFichier, "r");

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "nSommets") == 0)
		nbSommet = valeur;

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "oriente") == 0)
		oriente = valeur;

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "value") == 0)
		value = valeur;


	g = construire_graphe(nbSommet);

	g->valeur = value;

	for(int i = 0; i < nbSommet; i++){
		g->adj[i] = construire_cellule(i, -1);
	}

	g->mat = (int**)malloc(sizeof(int*)*nbSommet);
	for(int i = 0; i < nbSommet; i++){
		g->mat[i] = (int*)calloc(nbSommet, nbSommet);	
	}

	while(! feof(file)){
		fscanf(file, "%s", propriete);
		
		if(strcmp(propriete, "DEBUT_DEF_ARETES") == 0)
			boolDefAretes = 1;
	
		else if(strcmp(propriete, "FIN_DEF_ARETES") == 0)
			boolDefAretes = 0;

		else if(boolDefAretes){	
			fseek(file, strlen(propriete) * - 1, SEEK_CUR);

			int initial, terminal, poids;

			if(value){
				fscanf(file, "%d %d %d", &initial, &terminal, &poids);
			}
			else{
				fscanf(file, "%d %d", &initial, &terminal);
				poids = -1;
			}	

			inserer(g, initial,construire_cellule(terminal, poids));
			g->mat[initial][terminal] = poids;
			
			if(!oriente)
				inserer(g, terminal,construire_cellule(initial, poids));
				g->mat[terminal][initial] = poids;

			n_arete ++;
		}	
	}

	g->n_arete = n_arete;
	fclose(file);

	return g;
}

void construire_tableau_arete(graphe_t* g, arete_t* t_a){
	int compteur = 0;
	for(int i = 0; i < g->n_sommet; i ++){	
		cellule_t *sommet = g->adj[i];
		while(sommet->succ != NULL){
			int b = 1;
			for (int j = 0; j < g->n_arete; j++){
				if(t_a[j].sommet_u != sommet->succ->id_sommet || t_a[j].sommet_v != g->adj[i]->id_sommet)
					b &= 1;
				else
					b &= 0;
			}

			if(b){
				arete_t arete = {g->adj[i]->id_sommet, sommet->succ->id_sommet, sommet->succ->poids};
				t_a[compteur++] = arete;	
			}
			
			sommet = sommet->succ;
		}
	}	
}

acpm_t* creer_arbre_couvrant(const int nbSommet){
	acpm_t* r = (acpm_t*)malloc(sizeof(acpm_t));
	r->aretes = (arete_t*)malloc(sizeof(arete_t) * nbSommet -1);
	r->poids = 0;
	r->n_aretes = nbSommet -1;

	return r;
}

void tri_insertion(arete_t *arete, int taille){
    int j;
    arete_t en_cours;
 
    for (int i = 1; i < taille; i++) {
        en_cours = arete[i];

        for (j = i; j > 0 && arete[j - 1].poids > en_cours.poids; j--){
            arete[j] = arete[j - 1];
        }

        arete[j] = en_cours;
    }
}

void afficherAcpm(acpm_t* arbre){

	for (int i = 0; i < arbre->n_aretes; i++)
		printf("%2d:%2d (%d)\n", arbre->aretes[i].sommet_u, arbre->aretes[i].sommet_v, arbre->aretes[i].poids);

	printf("Poids : %d\n", arbre->poids);
}



acpm_t* generer_kruskal_ensemble(graphe_t* g){
	liste_ensemble_t *lE = creerEnsemble(g->n_sommet);

	arete_t tabArete[g->n_arete];

	construire_tableau_arete(g, (arete_t*)&tabArete);

	tri_insertion((arete_t*)&tabArete, g->n_arete);

	acpm_t *arbre = creer_arbre_couvrant(g->n_sommet);
	int cptAbre = 0;

	for (int i = 0; i < g->n_arete; i++){
		int u, v;
		u = tabArete[i].sommet_u;
		v = tabArete[i].sommet_v;
		
		sommet_ensemble_t *sU = lE->tabEns[u]->tete; 
		sommet_ensemble_t *sV = lE->tabEns[v]->tete;

		if(trouverEnsemble(sU) != trouverEnsemble(sV)){
			unionEnsemble(sU, sV);

			arbre->aretes[cptAbre] = tabArete[i];
			arbre->poids += arbre->aretes[cptAbre].poids;
			cptAbre++;
		}

	}

	return arbre;
}
