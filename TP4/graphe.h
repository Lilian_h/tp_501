#ifndef __GRAPHE_H__
#define __GRAPHE_H__
#include "cellule.h"



typedef struct Sommet_graphe{
	int valeur;
	int distance;
	int couleur;
	int dateDeb;
	int dateFin;
    struct Sommet_graphe *pred;
}sommet_graphe_t;

typedef struct {
	int n_sommet;
	int n_arete;
	int date;
	int valeur;
	int **mat;
	cellule_t **adj;
}graphe_t;


typedef struct Arete {
    int sommet_u;
    int sommet_v;
    int poids;
}arete_t;

typedef struct ACPM{
	arete_t* aretes;
	int n_aretes;
	int poids;
}acpm_t;



graphe_t* construire_graphe(const int);
void detruire_graphe(graphe_t*);
void inserer(graphe_t*, int, cellule_t*);
void afficher_graphe(graphe_t*);
graphe_t* construire_graphe_depuis_texte(const char *);

void construire_tableau_arete(graphe_t* g, arete_t* t_a);


/*KRUSKAL*/
acpm_t* creer_arbre_couvrant(const int);
void tri_insertion(arete_t *arete, int taille);
void afficherAcpm(acpm_t*);
acpm_t* generer_kruskal_ensemble(graphe_t*);

#endif
