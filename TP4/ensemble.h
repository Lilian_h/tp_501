#ifndef __ENSEMBLE_H__
#define __ENSEMBLE_H__

typedef struct SommetEnsemble{
    int valeur;
    struct Ensemble * ensemble;
    struct SommetEnsemble * succ;
} sommet_ensemble_t;

typedef struct Ensemble{
    sommet_ensemble_t * tete;
    sommet_ensemble_t * queue;
}ensemble_t;

typedef struct ListeEnsemble{
    ensemble_t** tabEns;
}liste_ensemble_t;


typedef struct Element
{
	int valeur;
	struct Element *s;
}element_t;


liste_ensemble_t * creerEnsemble(int n);
sommet_ensemble_t* trouverEnsemble(sommet_ensemble_t* sE);
void unionEnsemble(sommet_ensemble_t *sU, sommet_ensemble_t *sV);


#endif