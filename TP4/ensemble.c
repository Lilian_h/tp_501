#include "ensemble.h"
#include <stdlib.h>

liste_ensemble_t * creerEnsemble(int n){
	liste_ensemble_t* lE = (liste_ensemble_t*)malloc(sizeof(liste_ensemble_t));

	lE->tabEns = (ensemble_t**)malloc(sizeof(ensemble_t*) * n);

	
	for (int i = 0; i < n; i++){
		sommet_ensemble_t *sE = (sommet_ensemble_t*)malloc(sizeof(sommet_ensemble_t));
		sE->valeur = i;
		

		lE->tabEns[i] = (ensemble_t*)malloc(sizeof(ensemble_t) * 2);
		lE->tabEns[i]->tete = sE;
		lE->tabEns[i]->queue = sE;

		sE->ensemble = lE->tabEns[i];

	}

	return lE;
}

sommet_ensemble_t* trouverEnsemble(sommet_ensemble_t* sE){
	return(sE->ensemble->tete);
}

void unionEnsemble(sommet_ensemble_t *sU, sommet_ensemble_t *sV){
	sU->ensemble->queue->succ = sV->ensemble->tete;
	sU->ensemble->queue = sV->ensemble->queue;

	sommet_ensemble_t * sE = sV->ensemble->tete;
	while(sE != NULL)
	{
		sE->ensemble = sU->ensemble;
		sE = sE->succ;
	}
}