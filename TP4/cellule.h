#ifndef __CELLULE_H__
#define __CELLULE_H__

typedef struct Cellule{
	int id_sommet;
	int poids;
	struct Cellule *succ;
}cellule_t;

cellule_t* construire_cellule(const int, const int);
void detruire_cellule(cellule_t*);

#endif