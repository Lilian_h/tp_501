#ifndef __FILE_H__
#define __FILE_H__

typedef struct{
	int tete;
	int queue;
	int *element;
	int longueur; //Nombre max d'élément possible dans la liste
}file_t;

file_t* construire_file(int longueur);
void detruit_file(file_t *f);
void enfiler(file_t *f, int v);
int defiler(file_t *f);
int file_vide(file_t f);
int file_pleine(file_t f);

#endif 