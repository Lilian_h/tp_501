#include <stdio.h>
#include <stdlib.h>
#include "pile.h"

int capacite(pile_t* p){
	return p->taille;
}

int pile_pleine(pile_t* p){
	if(p->s + 1 == p->taille)
		return 1;
	else
		return 0;
}
int sommet(pile_t* p){
	return p->elements[p->s];
}

int pile_vide(pile_t* p){
	if(p->s == -1)
		return 1;
	else 
		return 0;
}

void empiler(pile_t* p, int v){
	if(p->s + 1 < p->taille){
		p->s++;
		p->elements[p->s] = v;
	}
}

int depiler(pile_t* p){
	if(!pile_vide(p)){
		p->s --;
		return p->elements[p->s + 1];
	}
	else
		return 0;
}
pile_t* construit_pile_vide(int taille){
	pile_t* p = (pile_t*) malloc(sizeof(pile_t*));
	p->taille = taille;
	p->s = -1;
	p->elements = (int*)malloc(sizeof(int) * taille);
	return p;
}

void detruit_pile(pile_t* p){
	free(p->elements);
	p->elements = NULL;
	free(p);
	p = NULL;	
}
