#include <stdio.h>
#include <stdlib.h>
#include "file.h"

file_t* construire_file(int longueur){
	file_t* f = (file_t*) malloc(sizeof(file_t*));
	
	f->tete = 0;
	f->queue = 0;
	f->element = (int*) malloc(sizeof(int) * longueur);
	f->longueur = longueur;
	return f;
}

void detruire_file(file_t* f){
	free(f->element);
	f->element = NULL;
	free(f);
	f = NULL;
}

int file_vide(file_t f){	
	return f.queue == f.tete;
}

int file_pleine(file_t f){
	return f.tete != f.queue + 1 % f.longueur;
}


void enfiler(file_t* f, int v){
	if(file_pleine(*f)){
		f->element[f->queue] = v;
	
		if(f->queue + 1 == f->longueur)
			f->queue = 0;
		else
			f->queue ++;
	}
}

int defiler(file_t* f){
	if(f->tete != f->queue){
		int v = f->element[f->tete];
		
		if(f->tete + 1 == f->longueur)
			f->tete = 0;
		else
			f->tete ++;
		
		return v;
	}
	else{
		return 0;
	}
}