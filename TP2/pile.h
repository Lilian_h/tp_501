#ifndef __PILE_H__
#define __PILE_H__

typedef struct
{
	int * elements;
	int taille;
	int s;
}pile_t;

int capacite(pile_t*);
int sommet(pile_t*);
int pile_vide(pile_t*);
int pile_pleine(pile_t*);
void empiler(pile_t*, int);
int depiler(pile_t* );
pile_t* construit_pile_vide(int);
void detruit_pile(pile_t*);

#endif