#ifndef __GRAPHE_H__
#define __GRAPHE_H__
#include "cellule.h"



typedef struct Sommet_graphe{
	int valeur;
	int distance;
	int couleur;
	int dateDeb;
	int dateFin;
    struct Sommet_graphe *pred;
}sommet_graphe_t;

typedef struct {
	int n_sommet;
	int n_arete;
	int date;
	int valeur;
	int **mat;
	cellule_t **adj;
}graphe_t;


typedef struct Arete {
    int sommet_u;
    int sommet_v;
    int poids;
}arete_t;

graphe_t* construire_graphe(const int);
void detruire_graphe(graphe_t*);
void inserer(graphe_t*, int, cellule_t*);
void afficher_graphe(graphe_t*);
graphe_t* construire_graphe_depuis_texte(const char *);

void construire_tableau_arete(graphe_t* g, arete_t* t_a);

sommet_graphe_t* parcours_largeur(graphe_t*, int);
void afficher_chemin(graphe_t*, int, int, sommet_graphe_t*);
sommet_graphe_t* parcours_profondeur_recursif(graphe_t*);
void visiter_pp(graphe_t *, int ,sommet_graphe_t*);
void afficher_parcours_profondeur(graphe_t*, int, int, sommet_graphe_t*);
sommet_graphe_t* parcours_profondeur_iteratif(graphe_t*);

#endif
