//
//  main.c
//  TP7_501
//
//  Created by Lilian Hollard on 27/11/2019.
//  Copyright © 2019 Lilian Hollard. All rights reserved.
//

#include <stdio.h>
#include "graphe.h"

int main(int argc, char *argv[]) {
    (void)argc;
    graphe_t* g = construire_graphe_depuis_texte(argv[1]);
   // afficher_graphe(g);
    sommet_graphe_t* p_l = parcours_largeur(g, 0);
    sommet_graphe_t* p_p = parcours_profondeur_recursif(g);

    for(int i = 0; i < g->n_sommet; i++){
        afficher_chemin(g, 0, i, p_l);
    }

    afficher_parcours_profondeur(g, 0, 1, p_p);
    
    return 0;
}
