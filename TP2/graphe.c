
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "graphe.h"
#include "file.h"
#include "pile.h"


/*
    Construire_graphe : 
    construction d'un graphe vide à partir de n_sommet
*/
graphe_t* construire_graphe(const int n_sommet){
	graphe_t* g = (graphe_t*) malloc(sizeof(graphe_t));

	g->n_sommet = n_sommet;
	g->n_arete = 0;
	g->date = 0;
	g->valeur = 0;

	g->adj = (cellule_t**) malloc(sizeof(cellule_t*) * n_sommet);

	return g;
}


/*
    detruire_graphe.
*/
void detruire_graphe(graphe_t* g){
	cellule_t* cellule_temp = NULL;

	for(int i = 0; i < g->n_sommet; i ++){
		while(g->adj[i] != NULL)
		{
			cellule_temp = g->adj[i]->succ;
			free(g->adj[i]);
			g->adj[i] = cellule_temp;
		}
	}

	free(g);
	g = NULL;
}

/*
    inserer :
    inserer un sommet dans un graphe depuis un indice dans la liste
    d'adjacence.
*/
void inserer(graphe_t* g, int indice, cellule_t* sommet)
{
	sommet->succ = g->adj[indice]->succ;
	g->adj[indice]->succ = sommet;
}

/*
    Anciennement : afficher_liste appelé iterativement
    Nouvelle version : 
        - > affichage de chaque cellule_t (aka sommet) de la liste d'adjacence
        - > continuer pour chaque indice du tableau de liste d'adj
*/
void afficher_graphe(graphe_t* g)
{
	for(int i = 0; i < g->n_sommet; i ++)
	{
		if(g->adj[i] != NULL)
		{
			cellule_t *sommet = g->adj[i];
			printf("[%d] -> ", sommet->id_sommet);

			while(sommet->succ != NULL)
			{
				if(g->valeur)
					printf("%d: poids(%d); ", sommet->succ->id_sommet, sommet->succ->poids);
				else
					printf("%d ", sommet->succ->id_sommet);
				sommet = sommet->succ;
			}	
		}
		printf("\n");	
	}
}

/* 
    Nouvelle version : identique à la précédente, mais réadapté 
	Ajout : matrice n x n pour Floyd warshall
*/
graphe_t* construire_graphe_depuis_texte(const char * nomFichier)
{
	FILE *file = NULL;
	char propriete[20];
	int valeur = 0;
	int n_arete = 0;
	int boolDefAretes = 0;

	int nbSommet = 0;
	int oriente = 0;
	int value = 0;

	graphe_t* g = NULL;
	
	file = fopen(nomFichier, "r");

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "nSommets") == 0)
		nbSommet = valeur;

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "oriente") == 0)
		oriente = valeur;

	fscanf(file, "%s %d", propriete, &valeur);
	if(strcmp(propriete, "value") == 0)
		value = valeur;


	g = construire_graphe(nbSommet);

	g->valeur = value;

	for(int i = 0; i < nbSommet; i++){
		g->adj[i] = construire_cellule(i, -1);
	}

	g->mat = (int**)malloc(sizeof(int*)*nbSommet);
	for(int i = 0; i < nbSommet; i++){
		g->mat[i] = (int*)calloc(nbSommet, nbSommet);	
	}

	while(! feof(file)){
		fscanf(file, "%s", propriete);
		
		if(strcmp(propriete, "DEBUT_DEF_ARETES") == 0)
			boolDefAretes = 1;
	
		else if(strcmp(propriete, "FIN_DEF_ARETES") == 0)
			boolDefAretes = 0;

		else if(boolDefAretes){	
			fseek(file, strlen(propriete) * - 1, SEEK_CUR);

			int initial, terminal, poids;

			if(value){
				fscanf(file, "%d %d %d", &initial, &terminal, &poids);
			}
			else{
				fscanf(file, "%d %d", &initial, &terminal);
				poids = -1;
			}	

			inserer(g, initial,construire_cellule(terminal, poids));
			g->mat[initial][terminal] = poids;
			
			if(!oriente)
				inserer(g, terminal,construire_cellule(initial, poids));
				g->mat[terminal][initial] = poids;

			n_arete ++;
		}	
	}

	g->n_arete = n_arete;
	fclose(file);

	return g;
}

sommet_graphe_t* parcours_largeur(graphe_t *g, int s)
{
	sommet_graphe_t *sommets = (sommet_graphe_t*)malloc(sizeof(sommet_graphe_t) * g->n_sommet);

	for (int i = 0; i < g->n_sommet; i++){
		sommets[i].valeur = i;
		sommets[i].couleur = 1;
		sommets[i].distance = 9999;
		sommets[i].pred = NULL;
	}

	sommets[s].couleur = 2;
	sommets[s].distance = 0;
	sommets[s].pred = NULL;
	
	file_t *file = construire_file(g->n_sommet);
	enfiler(file, s);

	while(!file_vide(*file)){
		int u = defiler(file);

		cellule_t *sommet_temp = g->adj[u]->succ;
		while(sommet_temp != NULL){	
			if(sommets[sommet_temp->id_sommet].couleur == 1){
				sommets[sommet_temp->id_sommet].couleur = 2;
				sommets[sommet_temp->id_sommet].distance += 1;
				sommets[sommet_temp->id_sommet].pred = &sommets[u];
				enfiler(file, sommet_temp->id_sommet);
			}
			
			sommet_temp = sommet_temp->succ;
		}
		sommets[u].couleur = 3;
		
	}
	return sommets;
}

void visiter_pp(graphe_t *g, int u, sommet_graphe_t *s)
{
	g->date ++;
	s[u].dateDeb = g->date;
	s[u].couleur = 2;

	cellule_t *sommet_temp = g->adj[s[u].valeur]->succ;
	while(sommet_temp != NULL)
	{
		if(s[sommet_temp->id_sommet].couleur == 1)
			{
				s[sommet_temp->id_sommet].pred = &s[u];
				visiter_pp(g, sommet_temp->id_sommet, s);
			}

		sommet_temp = sommet_temp->succ;
	}
	s[u].couleur = 3;
	g->date ++;
	s[u].dateFin = g->date;
}

sommet_graphe_t* parcours_profondeur_recursif(graphe_t *g)
{
	sommet_graphe_t *sommets = (sommet_graphe_t*)malloc(sizeof(sommet_graphe_t) * g->n_sommet);

	for (int i = 0; i < g->n_sommet; i++){
		sommets[i].valeur = i;
		sommets[i].couleur = 1;
		sommets[i].pred = NULL;
	}
	g->date = 0;
	for (int i = 0; i < g->n_sommet; i++){
		if(sommets[i].couleur == 1)
			visiter_pp(g, i, sommets);
	}
	return sommets;
}


void afficher_chemin(graphe_t* g, int d, int f, sommet_graphe_t* s){
	if(f == d){
		printf("> %5d\n", d);
	}
	else if(s[f].pred == NULL){
		printf("Pas de chemin : %d\n", f);
	}
	else{
		afficher_chemin(g, d, s[f].pred->valeur, s);
		printf("> %5d\n", s[f].valeur);
	}
}

void afficher_parcours_profondeur(graphe_t* g, int d, int f, sommet_graphe_t *s)
{
	if(d == f){
		printf(">Sommet: %d (d:%d)(f:%d)\n", s[d].valeur, s[d].dateDeb, s[d].dateFin);
	}
	else if(s[f].pred == NULL){
		printf("Pas de chemin\n");
	}
	else{
		afficher_parcours_profondeur(g, d, s[f].pred->valeur, s);
		printf(">Sommet: %d (d:%d)(f:%d)\n", s[f].valeur, s[f].dateDeb, s[f].dateFin);
	}
}

sommet_graphe_t* parcours_profondeur_iteratif(graphe_t* g){

}