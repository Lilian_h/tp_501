#ifndef __TABLE_HACHAGE_H__
#define __TABLE_HACHAGE_H__
#include "liste.h"


typedef struct HashTable{
    liste_t* l;
    int m;
}hash_table_t;

int k_division(int, unsigned long long);
unsigned long long convertirChEntier(char*, int);
hash_table_t* initialiser_table_hachage(int);
void detruire_table_hachage(hash_table_t*, int);
void afficher_table_hachage(hash_table_t*);
void inserer_table_h(hash_table_t*, char*, int);
cellule_t* rechercher_table_hachage(hash_table_t*, char*, int);
void supprimer_hachage(hash_table_t*, cellule_t*);

#endif
