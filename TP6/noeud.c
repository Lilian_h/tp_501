#include "noeud.h"
#include <stdlib.h>
#include <string.h>


noeud_t* initialiser_noeud(char* mot){
    noeud_t* res = (noeud_t*)malloc(sizeof(noeud_t));

    strcpy(res->mot, mot);
    res->fils_d = NULL;
    res->fils_g = NULL;
    res->pere = NULL;
    
    return res;
}

void detruire_noeud(noeud_t* n){
    free(n);
    n = NULL;
}
