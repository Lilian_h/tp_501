#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "cellule.h"
#include "liste.h"
#include "table_hachage.h"
#include "arbre.h"


int main(int argc, char* argv[]){
    (void)argc; // argc est inutilisé dans le code.
    FILE* fichier = NULL;
    hash_table_t* h = initialiser_table_hachage(11); 
    char mot[26];

    fichier = fopen(argv[1], "r");


    arbre_t* a = initialiser_arbre();
    while(!feof(fichier))
	{
		fscanf(fichier, "%s", mot);
        if(!feof(fichier)){
            inserer(a, initialiser_noeud(mot));
            inserer_table_h(h, mot, strlen(mot));
        }
    }

    printf("table de hachage :\n");
    afficher_table_hachage(h);
    printf("Arbre binaire de recherche :\n");
    afficher(a);

    return 0;
}

