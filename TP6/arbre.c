#include "arbre.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


arbre_t* initialiser_arbre(){
    arbre_t* res = (arbre_t*)malloc(sizeof(arbre_t));
    res->racine = NULL;
    return res;
}

arbre_t* initialiser_arbre_mot(char* mot){
    arbre_t* res = (arbre_t*)malloc(sizeof(arbre_t));
    noeud_t* n = initialiser_noeud(mot);
    res->racine = n;
    return res;
}

void detruire_arbre(arbre_t* a){

}

void inserer(arbre_t* a, noeud_t* n){
   if(rechercher_iteratif(a, n->mot) == NULL){
        noeud_t* y = NULL;
        noeud_t* x = a->racine;
        while(x != NULL){
            y = x;
            if(strcmp(n->mot, x->mot) == -1){ // si n->mot < droite->mot
                x = x->fils_g;
            }else{
                x = x->fils_d;
            }
        }
        n->pere = y;
        if(y == NULL){
            a->racine = n;
        }else if(strcmp(n->mot, y->mot) == -1){
            y->fils_g = n;
        }else{
            y->fils_d = n;
        }
    }
}


noeud_t* rechercher_iteratif(arbre_t* a, char* mot){
    noeud_t* n = a->racine;
    while(n != NULL && strcmp(mot, n->mot) != 0){
        if(strcmp(mot, n->mot) < 0){
            n = n->fils_g;
        }else{
            n = n->fils_d;
        }
    }
    return n;
}

void transplanter(arbre_t* a, noeud_t* u, noeud_t* v){
    if(u->pere == NULL){
        a->racine = v;
    }else{
        if(u == u->pere->fils_g){
            u->pere->fils_g = v;
        }else{
            u->pere->fils_d = v;
        }
        if(v != NULL){
            v->pere = u->pere;
        }
    }
}

noeud_t* noeud_minimum(noeud_t* n){
    noeud_t* x = n;
    while(x->fils_g != NULL){
        x = x->fils_g;
    }
    return x;
}

void supprimer(arbre_t* a, noeud_t* n){
    if(n->fils_g == NULL){
        transplanter(a, n, n->fils_d);
    }else if(n->fils_d == NULL){
        transplanter(a, n, n->fils_g);
    }else{
        noeud_t* y = noeud_minimum(n->fils_d);
        
        if(y->pere != n){
            transplanter(a, y, y->fils_d);
            y->fils_d = n->fils_d;
            y->fils_d->pere = y;
        }

        transplanter(a, n, y);
        y->fils_g = n->fils_g;
        y->fils_g->pere = y;
    }
}

void affiche_noeud_prefixe_recursif(noeud_t* n){
    if(n != NULL){
        affiche_noeud_prefixe_recursif(n->fils_g);
        printf("%s ", n->mot);
        affiche_noeud_prefixe_recursif(n->fils_d);
    }
}

void afficher(arbre_t* a){
    affiche_noeud_prefixe_recursif(a->racine);
    printf("\n");
}


