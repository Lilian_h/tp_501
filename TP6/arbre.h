#ifndef __ARBRE_H__
#define __ARBRE_H__
#include "noeud.h"

typedef struct Arbre{
    noeud_t* racine; //point d'entrée de l'arbre (pointant sur la racine)
}arbre_t;

arbre_t* initialiser_arbre(void);
arbre_t* initialiser_arbre_mot(char*);
void detruire_arbre(arbre_t*);
void inserer(arbre_t*, noeud_t*);
noeud_t* rechercher_iteratif(arbre_t*, char*);
noeud_t* noeud_minimum(noeud_t*);
void transplanter(arbre_t*, noeud_t*, noeud_t*);
void supprimer(arbre_t*, noeud_t*);
void affiche_noeud_prefixe_recursif(noeud_t*);
void afficher(arbre_t*);


#endif
