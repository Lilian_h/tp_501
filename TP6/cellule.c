#include "cellule.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void initialiser_cellule(cellule_t *c, char* mot){
    c->pred = NULL;
    c->succ = NULL;
    strcpy(c->mot, mot);
}

void detruire_celulle(cellule_t* c){
    free(c);
    c = NULL;
}
