#ifndef __NOEUD_H__
#define __NOEUD_H__

typedef struct Noeud{
    char mot[27];
    struct Noeud *pere;
    struct Noeud *fils_g;
    struct Noeud *fils_d;
}noeud_t;

noeud_t* initialiser_noeud(char*);
void detruire_noeud(noeud_t*);

#endif
