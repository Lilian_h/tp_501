#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <string.h>
#include "table_hachage.h"
#include "cellule.h"

int k_division(int m, unsigned long long c){
    return c % m;
}


unsigned long long convertirChEntier(char * mot, int taille_mot){
    int puissance = taille_mot -1;
    unsigned long long total = 0;

    for(int i = 0; i < taille_mot; i++){
        total += mot[i] * pow(128, puissance);
        puissance--;
    }
    return total;
}

hash_table_t* initialiser_table_hachage(int m){
    hash_table_t* h = (hash_table_t*)malloc(sizeof(hash_table_t));
    liste_t* table = (liste_t*)malloc(sizeof(liste_t)*m);
    h->m = m;
    h->l = table;
    for(int i = 0; i < m; i++){
        initialiser_liste(&table[i]);
    }
    return h;
}

void inserer_table_h(hash_table_t* h, char* mot, int taille_mot){
    unsigned long long ch = convertirChEntier(mot, taille_mot);

    int k = k_division(h->m, ch) - 1;

    cellule_t* c = (cellule_t*)malloc(sizeof(cellule_t));
    initialiser_cellule(c, mot);
    inserer_liste(&h->l[k], c);
}



void detruire_table_hachage(hash_table_t* h, int m){
    for(int i = 0; i < m; i++){
        if(h->l[i].tete != NULL){
            detruire_liste(&h->l[i]);
        }
    }
    free(h->l);
    h->l = NULL;
    free(h);
    h = NULL;
}

void afficher_table_hachage(hash_table_t* h){
    for(int i = 0; i < h->m; i++){
        if(h->l[i].tete != NULL){
            printf("[%d]->", i);
            afficher_liste(&h->l[i]);
        }else{
            printf("[%d]->/\n", i);
        }
    }
}

cellule_t* rechercher_table_hachage(hash_table_t* h, char* mot, int taille_mot){
    unsigned long long ch = convertirChEntier(mot, taille_mot);

    int k = k_division(h->m, ch) - 1;

    return rechercher_liste(&h->l[k], mot);
}

void supprimer_hachage(hash_table_t* h, cellule_t* c){
    unsigned long long ch = convertirChEntier(c->mot, (int)strlen(c->mot));

    int k = k_division(h->m, ch) - 1;

    supprimer_cellule(&h->l[k], c);
}
