#ifndef __CELLULE_H__
#define __CELLULE_H__

typedef struct Cellule {
    char mot[27];
    struct Cellule *pred;
    struct Cellule *succ;
}cellule_t;

void initialiser_cellule(cellule_t *, char*); 
void detruire_celulle(cellule_t*);

#endif
