#ifndef __TAS_H__
#define __TAS_H__
#include "graphe.h"

typedef struct Tas{
    sommet_graphe_t* t;
    int taille; 
    int longueur;
}tas_t;


int parent(int);
int gauche(int);
int droite(int);

void entasser_min(tas_t*, int);
void construire_tas_min(tas_t*);
int rechercher_tas(tas_t*, int);
/* File de priorité */
int minimum_tas(tas_t*);
sommet_graphe_t extrait_min_tas(tas_t*);
void diminuer_cle_tas(tas_t*, int, int);
void inserer_tas_min(tas_t*, int);


#endif