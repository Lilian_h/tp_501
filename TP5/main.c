//
//  main.c
//  TP7_501
//
//  Created by Lilian Hollard on 27/11/2019.
//  Copyright © 2019 Lilian Hollard. All rights reserved.
//

#include <stdio.h>
#include "graphe.h"

int main(int argc, char *argv[]) {
    (void)argc;
    graphe_t* g = construire_graphe_depuis_texte(argv[1]);
    acpm_t* arbre = prim_file_priorite(g);
    afficherAcpm(arbre);
    detruire_graphe(g);
    free(arbre);
    arbre = NULL;
    return 0;
}
