#include "tas.h"
#include <stdlib.h>
#include <stdio.h>

int parent(int i){
    return (i-1)/2; 
}

int gauche(int i){
    return ((i*2)+1);
}

int droite(int i){
    return ((i*2)+2);
}

void entasser_min(tas_t* tas, int i){		
	int g = gauche(i);
	int d = droite(i);
	int min;
	if(g < tas->taille && tas->t[g].distance < tas->t[i].distance){
		min = g;
	}
	else{
		min = i; 
    }

	if(d < tas->taille && tas->t[d].distance < tas->t[min].distance){
		min = d;
	}
	if(min != i){
		sommet_graphe_t temp = tas->t[i];
		tas->t[i] = tas->t[min];
		tas->t[min] = temp;		
		
		entasser_min(tas, min);
	}
}

void construire_tas_min(tas_t* tas){
	for(int i = (tas->longueur/2)-1; i >= 0; i--){
		entasser_min(tas, i);
	}
}

int rechercherTas(tas_t* tas, int valeur){
	for (int i = 0; i < tas->taille; i++){
		if(tas->t[i].valeur == valeur)
			return i;
	}
	return -1;
}



int minimum_tas(tas_t* tas){
	return tas->t[0].distance;
}


sommet_graphe_t extrait_min_tas(tas_t* tas){
	sommet_graphe_t min;
	
	if(tas->taille < 0){
		printf("Erreur(tas vide)");
    }
	else{
		min = tas->t[0];
		tas->t[0] = tas->t[tas->taille - 1];
		tas->t[tas->taille - 1].distance = 9999;
		tas->taille --;
		entasser_min(tas, 0);
	}
	
	return min;
}

void diminuer_cle_tas(tas_t* tas, int i, int cle){
	if(cle > tas->t[i].distance){
		printf("La nouvelle clé est plus grande\n");
    }
    else{
		tas->t[i].distance = cle;
		while(i > 0 && tas->t[parent(i)].distance > tas->t[i].distance){
			sommet_graphe_t temp = tas->t[i];
			tas->t[i] = tas->t[parent(i)];
			tas->t[parent(i)] = temp;
	
			i = parent(i);
		}
	}
}

void inserer_tas_min(tas_t* tas, int cle){
	tas->t[tas->taille].distance = 9999;
	tas->taille ++;
	diminuer_cle_tas(tas, tas->taille -1, cle);
}